import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = props => (
	<nav className="navbar navbar-default">
		<div className="container">
			<div className="navbar-header">
				<ul className="nav navbar-nav">
					<li><NavLink to="/home" activeStyle={{ color: 'red' }}>Home</NavLink></li>
					<li><NavLink to="/user/10"activeStyle={{ color: 'red' }}>User</NavLink></li>
				</ul>
				{/* { <ul>
					<li>{props.homeLink}</li>
				</ul> } */}
			</div>
		</div>
	</nav>
);
export default Header;
