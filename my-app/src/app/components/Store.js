import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import Promise from 'redux-promise-middleware';

const mathReducer = (state = {
	result: 1,
	lastValue: [],
}, action) => {
	switch (action.type) {
		case 'ADD':
			state = {
				...state,
				result: state.result + action.payload,
				lastValue: [...state.lastValue, action.payload],
			};
			break;
		case 'SUBTRACT':
			state = {
				...state,
				result: state.result - action.payload,
				lastValue: [...state.lastValue, action.payload],
			};
			break;
	}
	return state;
};

const userReducer = (state = {
	name: 'James',
	age: 24,
}, action) => {
	switch (action.type) {
		case 'SET_NAME_FULFILLED':
			state = {
				...state,
				name: action.payload,
			};
			break;
		case 'SET_AGE':
			state = {
				...state,
				age: action.payload,
			};
			break;
	}
	return state;
};


const store = createStore(
	combineReducers({ mathReducer, userReducer }),
	{},
	applyMiddleware(createLogger(), Promise()),
);

store.subscribe(() => {
 	// console.log('Store Updated', store.getState());
});

export default store