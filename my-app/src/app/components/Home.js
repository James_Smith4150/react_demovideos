import React, { Component } from 'react';

class Home extends Component {
	render() {
		return (
			<div className="col-xs-10 col-xs-offset-1">
				<h3>Home</h3>
			</div>
		);
	}

	// constructor(props) {
	// 	super();
	// 	this.state = {
	// 		age: props.Initailage,
	// 		status: 0,
	// 		homeLink: props.initailLinkName,
	// 	};
	// 	setTimeout(() => {
	// 		this.setState({
	// 			status: 1,
	// 		});
	// 	}, 3000);
	// 	console.log('Constructor');
	// }

	// componentWillMount() {
	// 	console.log('Component will mount');
	// }

	// componentDidMount() {
	// 	console.log('Component did mount');
	// }

	// componentWillReceiveProps(nextProps) {
	// 	console.log('Component will recieve props', nextProps);
	// }

	// shouldComponentUpdate(nextProps, nextState) {
	// 	console.log('Should Component update', nextProps, nextState);
	// 	// CODE TO DEMOSTRATE HOW PROPS WONT UPDATE ONCE YOU RETURN FALSE
	// 	// if(nextState.status === 1){
	// 	//    return false;
	// 	// }
	// 	return true;
	// }

	// componentWillUpdate(nextProps, nextState) {
	// 	console.log('Component will update', nextProps, nextState);
	// }

	// componentDidUpdate(prevProps, prevState) {
	// 	console.log('Component did update', prevProps, prevState);
	// }

	// componentWillUnmount() {
	// 	console.log('Component will unmount');
	// }

	// onMakeOlder() {
	// 	this.setState({
	// 		age: this.state.age + 1,
	// 	});
	// }
	// onMakeYounger() {
	// 	this.setState({
	// 		age: this.state.age - 1,
	// 	});
	// }
	// onChangeLink() {
	// 	this.props.changeLink(this.state.homeLink);
	// }

	// onHandleChange(event) {
	// 	this.setState({
	// 		homeLink: event.target.value,
	// 	});
	// }

	// render() {
	// 	return (
	// 		<div>
	// 			<p>Is a new Component</p>
	// 			<p>Your name is {this.props.name}, your age is {this.state.age}</p>
	// 			<p>Status: {this.state.status}</p>
	// 			<hr />
	// 			<button onClick={this.onMakeOlder.bind(this)} className="btn btn-primary">Make me Older!</button>
	// 			<button onClick={this.onMakeYounger.bind(this)} className="btn btn-primary">Make me Younger!</button>
	// 			<hr />
	// 			<button onClick={this.props.greet} className="btn btn-primary">Greet</button>
	// 			<hr />
	// 			<input
	// 				type="text"
	// 				value={this.state.homeLink}
	// 				onChange={event => this.onHandleChange(event)}
	// 			/>
	// 			<button onClick={this.onChangeLink.bind(this)} className="btn btn-primary">Change Header</button>
	// 		</div>
	// 	);
	// }
}

export default Home;
