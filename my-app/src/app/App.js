import React, { Component } from 'react';
import { connect } from 'react-redux';
import Promise from 'redux-promise-middleware';
// import { Switch } from 'react-router';
// import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import './App.css';
// import Header from './components/Header';
// import Home from './components/Home';
// import Root from './components/Root';
import User from './components/User';
import Main from './components/Main';

class App extends Component {
	changeUsername(newName) {
		this.setState({
			username: newName,
		});
	}

	render() {
		return (
			<div className="container">
				{/* <Main changeUsername={this.changeUsername.bind(this)} /> */}
				<Main changeUsername={() => this.props.setName('Anne')} />
				<User username={this.props.user.name} />
			</div>
		);
	}
}

// allows you to choose which properties to use from global store and assign them locally
const mapStateToProps = state => ({
	user: state.userReducer,
	math: state.mathReducer,
});

// allows you to choose which actions to use from global store
const mapDispatchToProps = dispatch => ({
	setName: (name) => {
		dispatch({
			type: 'SET_NAME',
			payload: new Promise((resolve, reject)=>{
				setTimeout(() => {
					resolve(name);
				}, 2000);
			}),
		});
	},
	// setName: (name) => {
	// 	setTimeout(() => {
	// 		dispatch({
	// 			type: 'SET_NAME',
	// 			payload: name,
	// 		});
	// 	}, 2000);
	// },
	// setName: (name) => {
	// 	dispatch({
	// 		type: 'SET_NAME',
	// 		payload: name,
	// 	});
	// },
});

// Add app to the end to tell it to hook up to that component.
export default connect(mapStateToProps, mapDispatchToProps)(App);

// store.dispatch({
// 	type: 'ADD',
// 	payload: 100,
// });
// store.dispatch({
// 	type: 'ADD',
// 	payload: 22,
// });
// store.dispatch({
// 	type: 'SUBTRACT',
// 	payload: 80,
// });
// store.dispatch({
// 	type: 'SET_AGE',
// 	payload: 30,
// });

// export default App;

// {/* <Router>
// <Root>
// 	<Switch>
// 		<Route exact path="/" component={() => <div />} />
// 		<Route path="/user/:id" component={User} />
// 		<Route path="/home" component={Home} />
// 	</Switch>
// </Root>
// </Router > */}

// constructor() {
// 	super();
// 	this.state = {
// 		homeLink: 'Home',
// 		homeMounted: true,
// 	};
// }
// onGreet() {
// 	alert('Hello!');
// }

// onChangeLinkName(newName)
// {
// 	this.setState({
// 		homeLink: newName,
// 	});
// }

// onChangeHomeMounted() {
// 	this.setState({
// 		homeMounted: !this.state.homeMounted,
// 	});
// }

// 	render() {
// 		let homeCmp = '';
// 		if (this.state.homeMounted) {
// 			homeCmp = (
// 				<Home
// 					name="James"
// 					Initailage={24}
// 					greet={this.onGreet}
// 					changeLink={this.onChangeLinkName.bind(this)}
// 					initialLinkName={this.state.homeLink}
// 				/>
// 			);
// 		}
// 		return (
// 			<div className="container">
// 				<BrowserRouter history={BrowserRouter}>
// 					<Route exact path="/" component={Home}>
// 						<Route exact path="/" component={Header} />
// 					</Route>
// 				</BrowserRouter>
// 				<div className="row">
// 					<div className="col-xs-10 col-xs-offset-1">
// 						<Header homeLink={this.state.homeLink} />
// 					</div>
// 				</div>
// 				<div className="row">
// 					<div className="col-xs-10 col-xs-offset-1">
// 						{homeCmp}
// 					</div>
// 				</div>
// 				<div className="row">
// 					<div className="col-xs-10 col-xs-offset-1">
// 						<button onClick={this.onChangeHomeMounted.bind(this)} className="btn btn-primary">(Un)Mount Home Component</button>
// 					</div>
// 				</div>
// 			</div>
// 		);
// 	}
// }

